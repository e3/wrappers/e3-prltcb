##############################################################################
## EtherCAT Motion Control Phase Reference Line Temperature Control Box IOC configuration

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="PRL")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

require ecmccfg 6.3.2
# needed by PID
require std 3.6.1
require calc
#- Choose motor record driver implementation
#-   ECMC_MR_MODULE="ecmcMotorRecord"  => ECMC native built in motor record support (Default)
#-   ECMC_MR_MODULE="EthercatMC"       => Motor record support from EthercatMC module (need to be loaded)
#- Uncomment the line below to use EthercatMC (and add optional EthercatMC_VER to startup.cmd call):
#- epicsEnvSet(ECMC_MR_MODULE,"EthercatMC")

# Epics Motor record driver that will be used:
# epicsEnvShow(ECMC_MR_MODULE)

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=6.3.1,stream_VER=2.8.10,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"

################################################################################
########Set EtherCAT bus startup timeout command added (defaults to 30seconds):

ecmcConfigOrDie "Cfg.SetEcStartupTimeout(200)"

##############################################################################
# Configure hardware:


epicsEnvSet("ECMC_SLAVE_NUM",-1)
epicsEnvShow("ECMC_SLAVE_NUM")


epicsEnvSet("TCB_NUM",1)
epicsEnvShow("TCB_NUM")

ecmcFileExist($(E3_CMD_TOP)/hw/ecmcTCB.cmd,1)
# TCB 1
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 2
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 3
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 4
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 5
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 6
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd

ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
#### Load PCB
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcPCB.cmd

# TCB 7
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 8
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 9
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 10
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 11
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
#Configure CU1521-0010 EtherCAT Medienkonverter (Singlemode)
#ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
#${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=CU1521-0010"

##### Continue configuring the other TCBs

# TCB 12
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 13
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 14
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 15
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 16
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 17
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd
# TCB 18
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.cmd

#ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
#${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=CU1521-0010"



# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
#ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

##############################################################################
## Loading records with correct naming
ecmcFileExist($(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB1_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB2_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB3_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB4_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB5_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB6_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB7_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB8_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB9_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB10_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB11_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB12_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB13_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB14_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB15_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB16_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB17_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB18_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.cmd, "TCB_EK1501_POS=$(TCB19_EK1501_POS)"

# Set paramaters
epicsEnvSet("P",    "PRL:")
epicsEnvSet("SLAVE_NUM", 10) # probably change this to be loaded in ecmcPRL
epicsEnvSet("HW_TYPE_IN", "EL3202-0010") # same as above
epicsEnvSet("INP",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM)-$(HW_TYPE_IN)-AI")
epicsEnvSet("INPA",    "$(INP)1")
epicsEnvSet("INPB",    "$(INP)2")

epicsEnvSet("HW_TYPE_OUT", "EL2502") #
epicsEnvSet("SLAVE_NUM_OUT", "2") #
epicsEnvSet("OUT", "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM_OUT)-$(HW_TYPE_OUT)-BO1")

iocshLoad("$(E3_CMD_TOP)/iocsh/PID_hw.iocsh", "P=$(P)")

iocInit()


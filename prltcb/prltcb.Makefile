#
#  Copyright (c) 2019 - 2020    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Luciano Carneiro Guedes
# email   : luciano.carneiroguedes@ess.eu
# Date    : 2022-08-23
# version : 1.0.0 
#


## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


############################################################################
#
# If you want to exclude any architectures:
#
############################################################################

EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=
APPDB:=$(APP)/Db
APPCMDS:=$(APP)/cmds


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard Db/*.db)
TEMPLATES += $(wildcard Db/*.template)
TEMPLATES += $(wildcard Db/*.substitutions)

# USR_INCLUDES += -I$(where_am_I)$(APPSRC)

############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard ../iocsh/*.iocsh)
SCRIPTS += $(wildcard hw/*.iocsh)


############################################################################
#
# If you have any .substitution files, then you should probably comment the
# following db target and uncomment the one that follows.
#
############################################################################

SUBS=$(wildcard Db/*.substitutions)


# #	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
#	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
#	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
#	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@



vlibs:

.PHONY: vlibs
